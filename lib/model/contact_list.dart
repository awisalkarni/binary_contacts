import 'contact.dart';

class ContactList {
  List<Contact> contacts = new List();

  ContactList({this.contacts});

  factory ContactList.fromJson(List<dynamic> parsedJson) {
    List<Contact> contacts = new List<Contact>();

    contacts = parsedJson.map((i) => Contact.fromJson(i)).toList();

    return new ContactList(
      contacts: contacts,
    );
  }
}
