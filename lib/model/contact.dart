import 'dart:convert';

import 'package:http/http.dart';

class Contact {
  String id;
  String first_name;
  String last_name;
  String email;
  String gender;
  String phone_no;
  DateTime date_of_birth;
  int dob_timestamp;
  
  Contact({
    this.id,
    this.first_name,
    this.last_name,
    this.email,
    this.gender,
    this.date_of_birth,
    this.phone_no,
    this.dob_timestamp,
  });

  factory Contact.fromJson(Map<String, dynamic> json) {

    final date_of_birth = json["date_of_birth"] == null ? 0 : json["date_of_birth"];
    final gender = json["gender"] == "male" ? "Male" : json["gender"];


    return new Contact(
      id: json['id'],
      first_name: json['first_name'],
      last_name: json['last_name'],
      email: json['email'],
      gender: gender,
      dob_timestamp: date_of_birth is int ? date_of_birth : int.parse(date_of_birth),
      date_of_birth: DateTime.fromMillisecondsSinceEpoch(date_of_birth is int ? date_of_birth : int.parse(date_of_birth)*1000),
      phone_no: json['phone_no'],
    );

  }

  remove(){

  }

  save() {
    print('saving user using a web service');
    _makePostRequest();
  }

  _makePostRequest() async {
    // set up POST request arguments
    String user_id = id != null ? "/"+id : "";
    String url = 'https://mock-rest-api-server.herokuapp.com/api/v1/user'+user_id;
    Map<String, String> headers = {"Content-type": "application/json"};
    String json;
    Response response;
    // make request
    if (id != null) {
      String json = '{"id": "$id", "first_name": "$first_name", "last_name": "$last_name", "email": "$email", "gender": "$gender", "date_of_birth": "$dob_timestamp", "phone_no": "$phone_no"}';
      response = await put(url, headers: headers, body: json);
    } else {
      String json = '{"first_name": "$first_name", "last_name": "$last_name", "email": "$email", "gender": "$gender", "date_of_birth": "$dob_timestamp", "phone_no": "$phone_no"}';
      response = await post(url, headers: headers, body: json);
    }

//    // check the status code for the result
    int statusCode = response.statusCode;
//    // this API passes back the id of the new item added to the body
    String body = response.body;
    print(body);
    print(json);
    // {
    //   "title": "Hello",
    //   "body": "body text",
    //   "userId": 1,
    //   "id": 101
    // }
  }
}
