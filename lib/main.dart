import 'package:binary_contacts/home.dart';
import 'package:flutter/material.dart';


void main() => runApp(ContactApp());

class ContactApp extends StatelessWidget {

//  final routes = <String, WidgetBuilder>{
//    loginPageTag: (context) => LoginPage(),
//    homePageTag: (context) => HomePage(),
//  };

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "ContactApp",
        theme: new ThemeData(
          primaryColor: Colors.blue,
        ),
        home: HomePage(),
//        routes: routes
    );
  }

}