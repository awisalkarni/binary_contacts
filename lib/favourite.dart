import 'package:binary_contacts/model/contact_list.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:binary_contacts/model/contact.dart';
import 'package:intl/intl.dart';


class FavouritePage extends StatefulWidget {
  final ContactList contactList;
  FavouritePage({this.contactList});

  @override
  State<StatefulWidget> createState() {
    return _FavouritePageState(contactList: contactList);
  }

}

class _FavouritePageState extends State<FavouritePage> {
  final ContactList contactList;
  ContactList _filteredRecords = new ContactList();
  _FavouritePageState({this.contactList});


  @override
  void initState() {
    this._filteredRecords.contacts = new List();
    _getContacts();
    super.initState();
  }

  void _getContacts() async {
    List contacts = await _loadFavContacts(contactList);
    setState(() {
      for (Contact contact in contacts) {
        if (!this._filteredRecords.contacts.contains(contacts)) {
          this._filteredRecords.contacts.add(contact);
        }

      }
    });
  }

  Future<List> _loadFavContacts(ContactList contactList) async{
    final prefs = await SharedPreferences.getInstance();
    // read
    final favStringList = prefs.getStringList('fav_user_ids') ?? [];
    print(favStringList);
    List filteredContactList = new List();

    for (int i = 0; i < contactList.contacts.length; i++) {
      if (favStringList.contains(contactList.contacts[i].id)) {
        if (!filteredContactList.contains(contactList.contacts[i])) {
          print(i);
          print(contactList.contacts[i].first_name);
          filteredContactList.add(contactList.contacts[i]);
        }
      }
    }

    return filteredContactList;


  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          elevation: 0.1,
          backgroundColor: Colors.blue,
          centerTitle: true,
          title: Text("Favourite Contacts"),
      ),
      backgroundColor: Colors.blue,
      body: ReorderableListView(
        padding: const EdgeInsets.only(top: 20.0),
        onReorder: (oldIndex, newIndex) {
          setState(() {
            _updateMyItems(oldIndex, newIndex);
          });
        },
        children: this
            ._filteredRecords
            .contacts
            .map((data) => _buildListItem(context, data))
            .toList(),
      ),


    );
  }

  void _updateMyItems(int oldIndex, int newIndex) {
    if(newIndex > oldIndex){
      newIndex -= 1;
    }

    final Contact item = this._filteredRecords.contacts.removeAt(oldIndex);
    this._filteredRecords.contacts.insert(newIndex, item);

  }

  Widget _buildListItem(BuildContext context, Contact contact) {
    return ListTile(
      key: ValueKey(contact.id),
      leading: Container(
        width: 100.0,
        height: 100.0,
        color: Colors.blue,
      ),
      trailing:  CircleAvatar(
        radius: 32,
        backgroundImage:
        NetworkImage('https://via.placeholder.com/150'),
      ),
      title: Text(contact.first_name + " " + contact.last_name,),
      subtitle: Text(contact.email),

    );
  }


}


