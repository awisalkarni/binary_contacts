import 'package:binary_contacts/add.dart';
import 'package:binary_contacts/detail.dart';
import 'package:binary_contacts/favourite.dart';
import 'package:binary_contacts/model/contact_list.dart';
import 'package:flutter/material.dart';
import 'package:binary_contacts/model/contact.dart';
import 'package:binary_contacts/edit.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:intl/intl.dart';

class HomePage extends StatefulWidget {
  @override
  _HomeState createState() {
    return _HomeState();
  }
}

class _HomeState extends State<HomePage> {
  final TextEditingController _filter = new TextEditingController();

  ContactList _records = new ContactList();
  ContactList _filteredRecords = new ContactList();

  String _searchText = "";
  String offlineJsonString = "";

  Icon _searchIcon = new Icon(Icons.search);

  Widget _appBarTitle = new Text("ContactApp");

  @override
  void initState() {
    super.initState();

    _records.contacts = new List();
    _filteredRecords.contacts = new List();
    _getOfflineContacts();
//    _getContacts();
  }

  Future<ContactList> fetchUserFromAPI() async {
    final response = await http
        .get('https://mock-rest-api-server.herokuapp.com/api/v1/user');

//    print(response.body.toString());

    List responseJson = json.decode(response.body.toString())['data'];
    offlineJsonString = response.body.toString();
    _saveOfflineJson();

    ContactList contactList = new ContactList.fromJson(responseJson);

    return contactList;
  }

  void _getOfflineContacts() async {
    ContactList contacts = await _readOfflineJson();
    setState(() {
      if (contacts.contacts != null) {
        for (Contact contact in contacts.contacts) {
          this._records.contacts.add(contact);
          this._filteredRecords.contacts.add(contact);
        }
      }

      _getContacts();
    });
  }

  void _getContacts() async {
    ContactList contacts = await fetchUserFromAPI();
    setState(() {
      //flush first
      _resetRecords();

      for (Contact contact in contacts.contacts) {
//        print(contact.first_name);

//        print(
//            DateTime.fromMillisecondsSinceEpoch(contact.dob_timestamp * 1000));
        this._records.contacts.add(contact);
        this._filteredRecords.contacts.add(contact);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildBar(context),
      backgroundColor: Colors.blue,
      body: _buildList(context),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => new AddPage()));
        },
        child: Icon(Icons.add),
        backgroundColor: Colors.blueAccent,
      ),
      resizeToAvoidBottomPadding: false,
    );
  }

  Widget _buildBar(BuildContext context) {
    return AppBar(
        elevation: 0.1,
        backgroundColor: Colors.blue,
        centerTitle: true,
        title: _appBarTitle,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.favorite),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => FavouritePage(contactList: _records,)));
            },
          )
        ],
        leading: IconButton(icon: _searchIcon, onPressed: _searchPressed));
  }

  Widget _buildList(BuildContext context) {
    if (!(_searchText.isEmpty)) {
      _filteredRecords.contacts = new List();
      for (int i = 0; i < _records.contacts.length; i++) {
        if (_records.contacts[i].first_name
                .toLowerCase()
                .contains(_searchText.toLowerCase()) ||
            _records.contacts[i].last_name
                .toLowerCase()
                .contains(_searchText.toLowerCase())) {
          _filteredRecords.contacts.add(_records.contacts[i]);
        }
      }
    }

    return ListView(
      padding: const EdgeInsets.only(top: 20.0),
      children: this
          ._filteredRecords
          .contacts
          .map((data) => _buildListItem(context, data))
          .toList(),
    );
  }

  Widget _buildListItem(BuildContext context, Contact contact) {
    return Slidable(
      actionPane: SlidableDrawerActionPane(),
      actionExtentRatio: 0.25,
      child: Container(
        color: Colors.blueAccent,
        child: Card(
          key: ValueKey(contact.first_name),
          elevation: 8.0,
          margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
          child: Container(
            decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
            child: ListTile(
              contentPadding:
                  EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
              leading: Container(
                  padding: EdgeInsets.only(right: 12.0),
                  decoration: new BoxDecoration(
                      border: new Border(
                          right: new BorderSide(
                              width: 1.0, color: Colors.white24))),
                  child: Hero(
                      tag: "avatar_" + contact.first_name,
                      child: CircleAvatar(
                        radius: 32,
                        backgroundImage:
                            NetworkImage('https://via.placeholder.com/150'),
                      ))),
              title: Text(
                contact.first_name + " " + contact.last_name,
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              ),
              subtitle: Row(
                children: <Widget>[
                  new Flexible(
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                        RichText(
                          text: TextSpan(
                            text: contact.email,
                            style: TextStyle(color: Colors.white),
                          ),
                          maxLines: 3,
                          softWrap: true,
                        ),
                        Text(
                          'Date of Birth: ' +
                              new DateFormat("dd/MM/yyyy")
                                  .format(contact.date_of_birth),
                          style: TextStyle(color: Colors.white),
                        ),
                      ]))
                ],
              ),
              trailing: Icon(Icons.keyboard_arrow_right,
                  color: Colors.white, size: 30.0),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => new DetailPage(
                              contact: contact,
                            )));
              },
            ),
          ),
        ),
      ),
      secondaryActions: <Widget>[
        IconSlideAction(
          caption: 'Edit',
          color: Colors.orange,
          icon: Icons.edit,
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (builder) => new EditPage(
                          contact: contact,
                        )));
          },
        ),
        IconSlideAction(
          caption: 'Delete',
          color: Colors.red,
          icon: Icons.delete,
          onTap: () {
            setState(() {
              _filteredRecords.contacts.remove(contact);
            });
          },
        ),
      ],
    );
  }

  _HomeState() {
    _filter.addListener(() {
      if (_filter.text.isEmpty) {
        setState(() {
          _searchText = "";
          _resetRecords();
        });
      } else {
        setState(() {
          _searchText = _filter.text;
        });
      }
    });
  }

  Future<ContactList> _readOfflineJson() async {
    final prefs = await SharedPreferences.getInstance();
    final key = 'contacts_key';
    final value = prefs.getString(key) ?? 0;

    List responseJson = value != 0 ? json.decode(value)['data'] : List();

//    print('read: $value');
    ContactList contactList = new ContactList.fromJson(responseJson);
    return value != 0 ? contactList : ContactList();
  }

  _saveOfflineJson() async {
    final prefs = await SharedPreferences.getInstance();
    final key = 'contacts_key';
    final value = json;
    prefs.setString(key, offlineJsonString);
//    print('saved $value');
  }

  void _resetRecords() {
    this._filteredRecords.contacts = new List();
    for (Contact contact in _records.contacts) {
      this._filteredRecords.contacts.add(contact);
    }
  }

  void _searchPressed() {
    setState(() {
      if (this._searchIcon.icon == Icons.search) {
        this._searchIcon = new Icon(Icons.close);
        this._appBarTitle = new TextField(
          controller: _filter,
          style: new TextStyle(color: Colors.white),
          decoration: new InputDecoration(
            prefixIcon: new Icon(Icons.search, color: Colors.white),
            fillColor: Colors.white,
            hintText: 'Search by name',
            hintStyle: TextStyle(color: Colors.white),
          ),
        );
      } else {
        this._searchIcon = new Icon(Icons.search);
        this._appBarTitle = new Text("ContactApp");
        _filter.clear();
      }
    });
  }
}
